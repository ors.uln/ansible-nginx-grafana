# ansible-nginx-grafana



## Описание

Ansible-плейбук, который устанавливает на чистый серверный Ubuntu (20.04) Docker, docker-compose.
Отключает парольную аутентификацию по ssh.
В контейнерах докер развертывает nginx и grafana.
Nginx настраивает как обратный прокси для grafana.
В результате Grafana становиться доступна по адресу http://x.x.x.x:80 (x.x.x.x - внешний ip адрес хоста)


```sh
Что бы выполнить установку на локальной машине запустите:
ansible-playbook local-playbook.yml
```

```sh
Что бы выполнить установку на удаленный хост:
В файле hosts внесите адрес удаленного хоста (server1 ansible_host=xxx)
Добавьте ssh ключ вашего пользователя на удаленный хост:
ssh-keygen
ssh-copy-id user-name@host-address
Запустите плейбук:
ansible-playbook -i hosts remote-playbook.yml
```

Запускался и адаптирован для Ansible версий 3.2.0 (2.9) и 5.7.0